# Installed

- copy .env.example to .env
- npm install

# run

- npm run socket


# Client socket

  Testing frond socket path : ''/socket'
  Testing code :

  `       <script src="/socket.io/socket.io.js"></script>
          <script>
           var socket = io.connect('http://hostSocket');
           socket.on('connect', (data) => {
              socket.emit('agent', { section: 'CURRENTAGG' });
              socket.on('packed', (packed) => {
                console.log("packed", packed);
              });
           });
          </script>`
