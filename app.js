'use strict';

require('dotenv-safe').config({
    allowEmptyValues: true,
    example: './.env.example'
});

const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const serverPort = 4200;
const watch = require('./socket');

const allowHosts = process.env.ALLOW_HOST_SOCKET.split(',');
const interval = 500;

app.use(express.static(__dirname + '/node_modules'));
app.get('/', (req, res,next) => {
  res.status(200).json({
    name: "service-socket",
    version: "1.0.0",
    time: new Date().getTime()
  });
});
app.get('/socket', (req, res) => {
    res.sendFile(__dirname + '/public/socket.html');
});

io.on('connection', (client) => {
    console.info('Connected to client.');
    client.emit('Connected to service socket.');
    if(-1 === allowHosts.indexOf(client.handshake.headers.host)){
      console.log("Client disconnect");
      return client.disconnect();
    }
    client.on('agent', (agent) => {
      setInterval(()=>{
        if(watch[agent.section]){
            client.emit('packed', watch[agent.section]);
        }
      }, interval);
    });
});

io.on('disconnect', async () => {
    console.log("Client disconnect");
});

server.listen(serverPort, () => {
    console.log(`Socket is running on port ${serverPort}`);
});
