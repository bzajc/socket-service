const request = require("request");
const _ = require("lodash");

const CCC = {};
_.set(CCC, 'STATIC.TYPE', {
    'TRADE': '0',
    'FEEDNEWS': '1',
    'CURRENT': '2',
    'LOADCOMPLATE': '3',
    'COINPAIRS': '4',
    'CURRENTAGG': '5',
    'TOPLIST': '6',
    'TOPLISTCHANGE': '7',
    'ORDERBOOK': '8',
    'FULLORDERBOOK': '9',
    'ACTIVATION': '10',
    'FULLVOLUME': '11',
    'TRADECATCHUP': '100',
    'NEWSCATCHUP': '101',
    'TRADECATCHUPCOMPLETE': '300',
    'NEWSCATCHUPCOMPLETE': '301',
    'UNAUTHORIZED': '401'
});

async function getSubs() {
    return new Promise((res, rej) => {
        request.get('https://min-api.cryptocompare.com/data/top/totalvol?limit=100&page=0&tsym=USD', { json: true }, (err, resp, body) => {
            if(err) {
                return rej(err);
            }

            let subs = body.Data.map(pair => pair.ConversionInfo.SubsNeeded.join(",")).join(",").split(",");
            let uniq = _.uniq(subs);

            res(uniq);
        });
    })
}


(async ()=> {
    let subs = await getSubs();
    var socket = require('socket.io-client')('https://streamer.cryptocompare.com', {secure: true});
    socket.on('connect', function(){
        console.log(arguments)

        socket.emit('SubAdd', {
            subs: [subs[0],subs[1]],
            auth: ""
        });
    });

    socket.on('disconnect', function(){
        console.log(arguments);
    });
    socket.on('m', function(message){
        var messageType = message.substring(0, message.indexOf("~"));
        console.log(message);
        switch (messageType) {
            case CCC.STATIC.TYPE.TRADE:
                // streamerUtilitiesInstance.emitCurrentTrade('TradeMessage', message);
                break;
            case CCC.STATIC.TYPE.CURRENT:
               // streamerUtilitiesInstance.emitCurrentMessage('CurrentMessage', message);
                break;
            case CCC.STATIC.TYPE.CURRENTAGG:
                //streamerUtilitiesInstance.emitCurrentMessage('CurrentAggMessage', message);
                break;
            case CCC.STATIC.TYPE.ORDERBOOK:
                //streamerUtilitiesInstance.emitOrderBookMessage('OrderBookMessage', message);
                break;
            case CCC.STATIC.TYPE.FULLORDERBOOK:
               // streamerUtilitiesInstance.emitFullOrderBookMessage('FullOrderBookMessage', message);
                break;
            case CCC.STATIC.TYPE.FULLVOLUME:
               // streamerUtilitiesInstance.emitFullVolumeMessage('FullVolumeMessage', message);
                break;
            case CCC.STATIC.TYPE.ACTIVATION:
               // streamerUtilitiesInstance.emitActivationMessage('ActivationMessage', message);
                break;
            case CCC.STATIC.TYPE.LOADCOMPLATE:
                /*if (!isStreaming && !alreadyStopped) {
                    streamerUtilitiesInstance.stopStreaming();
                    alreadyStopped = true;
                }*/
                break;
            case CCC.STATIC.TYPE.UNAUTHORIZED:
                console.log(message);
                break;
        }
    });


})();
