const request = require("request");
const _ = require("lodash");
const watch = {
  CURRENTAGG: null
};

const CCC = {};
_.set(CCC, 'STATIC.TYPE', {
    'TRADE': '0',
    'FEEDNEWS': '1',
    'CURRENT': '2',
    'LOADCOMPLATE': '3',
    'COINPAIRS': '4',
    'CURRENTAGG': '5',
    'TOPLIST': '6',
    'TOPLISTCHANGE': '7',
    'ORDERBOOK': '8',
    'FULLORDERBOOK': '9',
    'ACTIVATION': '10',
    'FULLVOLUME': '11',
    'TRADECATCHUP': '100',
    'NEWSCATCHUP': '101',
    'TRADECATCHUPCOMPLETE': '300',
    'NEWSCATCHUPCOMPLETE': '301',
    'UNAUTHORIZED': '401'
});

async function getSubs() {
    return new Promise((res, rej) => {
        request.get(process.env.GET_SUB_URL, { json: true }, (err, resp, body) => {
            if(err) {
                return rej(err);
            }

            let subs = body.Data.map(pair => pair.ConversionInfo.SubsNeeded.join(",")).join(",").split(",");
            let uniq = _.uniq(subs);

            res(uniq);
        });
    })
}


(async ()=> {
    let subs = await getSubs();
    const client = require('socket.io-client')(process.env.GET_SOCKET_URL, {secure: true});
    client.on('connect', () => {
        client.emit('SubAdd', {
            subs: [subs[0],subs[1]],
            auth: ""
        });
    });
    client.on('disconnect', console.log);
    client.on('m', (message) => {
        const messageType = message.substring(0, message.indexOf("~"));
        message = message.split('~');
        switch (messageType) {
            case CCC.STATIC.TYPE.TRADE:
                watch.TRADE = message;
                break;
            case CCC.STATIC.TYPE.CURRENT:
                watch.CURRENT = message;
                break;
            case CCC.STATIC.TYPE.CURRENTAGG:
                watch.CURRENTAGG = message;
                break;
            case CCC.STATIC.TYPE.ORDERBOOK:
                break;
            case CCC.STATIC.TYPE.FULLORDERBOOK:
                break;
            case CCC.STATIC.TYPE.FULLVOLUME:
                break;
            case CCC.STATIC.TYPE.ACTIVATION:
                break;
            case CCC.STATIC.TYPE.LOADCOMPLATE:
                break;
            case CCC.STATIC.TYPE.UNAUTHORIZED:
                console.warn(message);
                break;
        }
    });
})();

module.exports = watch;
